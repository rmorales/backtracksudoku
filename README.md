# Heurística en Sudoku

Este proyecto incluye dos cuadernos que permiten observar y comparar la aplicación de un algoritmo computacional de búsqueda por avance y retroceso (_backtracking_) para resolver un problema de Sudoku con el uso de un algoritmo que implementa una heurística (selección del cuadro vacío con menos opciones).

De manera general, el proyecto busca mostrar la diferencia entre la fuerza bruta computacional, que resuelve el problema por búsqueda exhaustiva gracias a la capacidad de cómputo disponible, y el uso de técnicas de inteligencia artificial que buscan una solución suficientemente buena suficientemente rápido.

El proyecto usa `ipycanvas` para desplegar el tablero de Sudoku y representar el proceso de búsqueda.

El proyecto se ha probado usando Linux 22.04 y la versión 3-2023.07-2 de Anaconda usando los siguientes comandos en el directorio donde se descargó el código:

```
conda create --name sudoku python=3
conda activate sudoku
conda install -c conda-forge ipycanvas
conda install -c conda-forge jupyterlab
jupyter-lab
```

